<?php

use Classes\Route;
use Controller\HomeController;



Route::get('/', function () {
    header('Location:/home');
});
Route::get('/home', function () {
    HomeController::homeView();
});
Route::get('/register', function () {
    HomeController::registerView();
});

Route::get('/login', function () {
    HomeController::loginView();
});
Route::get('/dashboard', function () {
    HomeController::dashboard();
});

Route::get('/logout', function () {
    HomeController::logoutUser();
});

Route::post('/register/user', function () {
    HomeController::register($_POST);
});
Route::post('/login/user', function () {
    HomeController::loginUser($_POST);
});

Route::get('/results', function () {
    HomeController::results($_GET);
});
