<?php

namespace Controller;
error_reporting(0);
use Classes\Database;

class Controller extends Database
{
    
    public static function view($viewName, $key = null, $data = null)
    {

        if ($key != null && $data != null) {
            session_start();
            $_SESSION[$key]=$data;
          
        }
        require_once("./views/$viewName.php");
    }
}
