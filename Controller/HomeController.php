<?php

namespace Controller;

use Classes\Validator;

class HomeController extends Controller
{

    public static function homeView()
    {
        $mainTypes = self::query("SELECT * FROM types WHERE type_sort='main'");
        //
        return self::view('home', 'mainTypes', $mainTypes);
    }
    public static function registerView()
    {
        $types = self::query('SELECT * FROM types');

        return self::view('register', 'types', $types);
    }
    public static function register($request)
    {

        $validator = new Validator($request);

        if ($validator->registerValidation()) {
            unset($request['repeat_password']);
            $request['password'] = password_hash($request['password'], PASSWORD_BCRYPT);

            self::insert('users', $request);
            session_start();
            $_SESSION['successMsg'] = "Succesfully created user";
            header('Location:/login');
        }
    }
    public static function loginView()
    {
        return self::view('login');
    }

    public static function loginUser($request)
    {
        if (!empty($request['email']) && !empty($request['password'])) {
            $email = $request['email'];
            $password = $request['password'];

            $user = self::selectOne("SELECT id,name,email,password FROM users WHERE email='$email'");
            
            if ($user) {
                if (password_verify($password, $user->password)) {

                    session_start();
                    $_SESSION['user'] = ['name'=>$user->name,'email'=>$user->email,'id'=>$user->id];

                    if (isset($_SESSION['searchParams'])) {
                        header('Location:/results');
                    } else {
                        header('Location:/dashboard');
                    }
                } else {
                    self::view('login', 'error', 'Incorect Password');
                }
            } else {
                self::view('login', 'error', 'Email  dosent exist!');
            }
        } else {

            self::view('login', 'error', 'Email and password are required!');
        }
    }
    public static function dashboard()
    {
        return self::view('dashboard');
    }
    public static function logoutUser()
    {
        session_start();
        unset($_SESSION["user"]);
        header('Location:/home');
    }

    public static function results($search)
    {

        session_start();
      //proveri dali ima logirano user 
        if (!isset($_SESSION['user'])) {
            $_SESSION['searchParams'] = $search;
            header("Location:/login");
            die();
        }
        //proveri dali userot pred da se logira ja iskoristil search formata
        if(isset($_SESSION['searchParams'])){
            $search= $_SESSION['searchParams'];
            unset($_SESSION['searchParams']);
         
        }

        $types = self::query("SELECT name,id FROM types ");
        $data = [];

        /// sekoj tip na user stavi go vo niza zavisno od toa kakov tip e odnosno dali e tip pod tip itn.
        foreach ($types as $a => $type) {

            $language = self::selectOne("SELECT types.id,types.name,count(users.id) as numOfUsers,types.type_sort,types.name,types.belongesToType,types.belongesToSubType,types.mainType
             FROM users RIGHT JOIN types ON users.type_id=types.id
              WHERE types.id=$type->id OR types.belongesToType=$type->id OR types.belongesToSubType=$type->id or types.mainType=$type->id ");

            switch ($language->type_sort) {
                case "main":
                    $data['main'][] = $language;
                    break;
                case "mid":
                    $data['mid'][] = $language;
                    break;
                case "sub":
                    $data['sub'][] = $language;
                    break;
                case "min":
                    $data['min'][] = $language;
                    break;
               
            }
        }
        $searchResults = [];
    
        if (isset($search['search']) && isset($search['type_id'])) {
            $searchUser = $search['search'];
            $searchType = $search['type_id'];
            if (empty($searchUser) && $searchType == 0) {
                $searchResults = self::query("SELECT users.name as name,users.email as email,types.name as type FROM users JOIN types ON users.type_id=types.id ");
            } elseif (empty($searchUser) && $searchType != 0) {
                $searchResults = self::query("SELECT users.name as name,users.email as email,types.name as type FROM users JOIN types ON users.type_id=types.id 
            WHERE types.mainType=$searchType");
            } else {
                $searchResults = self::query("SELECT users.name as name,users.email as email,types.name as type FROM users JOIN types ON users.type_id=types.id 
            WHERE (types.mainType=$searchType OR types.id=$searchType) AND(users.email LIKE '%$searchUser%' OR users.name LIKE '%$searchUser%')");
            }
        }
        $finalData = ['searchResults' => $searchResults, 'allResults' => $data];


        self::view('/results', 'results', $finalData);
    }
}
