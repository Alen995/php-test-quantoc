<?php

namespace Classes;

class Route
{
    public static $validGetRoutes = [];
    public static $validPostRoutes = [];

    public static function post($route, $function)
    {
        self::$validPostRoutes[] = $route;

        if ($_SERVER['REQUEST_URI'] == $route) {

            if ($_SERVER['REQUEST_METHOD'] === "POST") {
                return    $function->__invoke();
            } else {
                die("This Route Requires POST method");
            }
        }
    }

    public static function get($route, $function)
    {

        self::$validGetRoutes[] = $route;

        $currentUrl = explode('?', $_SERVER['REQUEST_URI']);

        if ($currentUrl[0] == $route) {

            if ($_SERVER['REQUEST_METHOD'] === "GET") {
                return $function->__invoke();
            } else {
                die("This Route Requires GET method");
            }
        }
    }
}
