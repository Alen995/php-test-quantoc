<?php

namespace Classes;

class Validator extends Database
{
    protected $data = [];
    protected $errors = [];

    public function __construct($post_data)
    {
        $this->data = $post_data;
    }


    public function registerValidation()
    {
       
        $this->validateName();

        $this->validateEmail();

        $this->validatePassword();

        $this->validateRepeatPassword();

        $this->validateUserType();

        if (empty($this->errors)) {
            return true;
        } else {
            session_start();
            $_SESSION['validationErrors'] = $this->errors;
           
            header('Location:/register');
            die();
        }
    }




    protected function validateName()
    {
        $val = trim($this->data['name']);
        if (empty($val)) {
            $this->addError('name', 'name cannot be empty');
        } else {
            if (!preg_match('/^[a-zA-Z]{3,24}$/', $val)) {
                $this->addError('name', 'name must be 6-24 chars');
            }
        }
    }
    protected function validateUserType()
    {
        $val = trim($this->data['type_id']);
        if ($val==0) {
            $this->addError('type_id', 'user type cannot be empty');
        }
    }
    protected function validateEmail()
    {

        $val = trim($this->data['email']);
        if (empty($val)) {
            $this->addError('email', 'email cannot be empty');
        } elseif (!filter_var($val, FILTER_VALIDATE_EMAIL)) {
            $this->addError('email', 'email is not valid');
        } elseif ($this->emailExists()) {
            $this->addError('email', 'email already exists');
        }
    }
    protected function validatePassword()
    {
        $val = trim($this->data['password']);
        if (empty($val)) {
            $this->addError('password', 'password cannot be empty');
        } else {
            if (!preg_match('/^[a-zA-Z0-9]{6,24}$/', $val)) {
                $this->addError('password', 'password must be 6-24 chars');
            }
        }
    }
    protected function validateRepeatPassword()
    {
        $val = trim($this->data['repeat_password']);
        if (empty($val)) {
            $this->addError('password2', 'password cannot be empty');
        } else {
            if ($val != $this->data['password']) {
                $this->addError('password2', 'password 2 must be the same');
            }
        }
    }

    protected function validateUser()
    {
        $this->connect();
        $email = $this->data['email'];
        $password = $this->data['password'];
        $sql = "SELECT * FROM users WHERE email='$email'AND password='$password'";
        $stmt = $this->pdo->prepare($sql);
        $stmt->execute();

        $user = $stmt->fetch();
        if (!$user) {
            $this->addError('user','User dosent exist');
        }else 
        return $user;
    }

    protected function emailExists()
    {
        $this->connect();
        $email = $this->data['email'];

        $emailDb = self::selectOne("SELECT * FROM users WHERE email='$email'");
        if (empty($emailDb)) {
            return false;
        } else {
            return true;
        }
     
    }
    protected function addError($key, $value)
    {
        $this->errors[$key] = $value;
    }



   
   
 
 

}
