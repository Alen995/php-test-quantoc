<?php
namespace Classes;

use PDO;
use PDOException;


class Database
{
    protected static $pdo = NULL;

    protected static $host = 'localhost';
    protected static $driver = 'mysql';
    protected static $dbname = 'appDb';
    protected static $username = 'root';
    protected static $password = '';


    public static function connect()
    {
        $dns = self::$driver . ":host=" . self::$host . ";dbname=" . self::$dbname;

        if (self::$pdo == null) {
            try {
                self::$pdo = new PDO($dns, self::$username, self::$password);
            } catch (PDOException $e) {
                return $e->getMessage();
            }
        }
    }

    public static function query($query, $params = array())
    {
        self::connect();

        $stmt = self::$pdo->prepare($query);

        $stmt->execute($params);

        if (explode(' ', $query)[0] == 'SELECT') {
            $data = $stmt->fetchAll(\PDO::FETCH_OBJ);
            return $data;
        }
    }
    public static function selectOne($query)
    {
        self::connect();
        $stmt = self::$pdo->prepare($query);
        $stmt->execute();
        $data = $stmt->fetch(\PDO::FETCH_OBJ);
        return $data;
    }
    public static function insert($tableName, array $data)
    {
        self::connect();
        $key = array_keys($data);
        $val = array_values($data);

        $sql = "INSERT INTO $tableName (" . implode(', ', $key) . ") VALUES('" . implode("', '", $val) . "')";

        $stmt = self::$pdo->prepare($sql);


        $stmt->execute();
    }
}
