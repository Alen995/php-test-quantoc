<?php require_once __DIR__ . "/layouts/head.php";
error_reporting(0);

?>
<div class="container my-4">

    <div class="row">

        <div class="col-12 offset-0 col-md-6 offset-md-3 border shadow px-5 py-3">
            <h3 class='text-center mb-3'>Register</h3>
            <form action='/register/user' method='POST'>
                <div class="form-group">

                    <label for="email">Email address <?php if (isset($_SESSION['validationErrors']['email'])) { ?>
                            <span class="alert alert-danger text-center  p-1"><?= $_SESSION['validationErrors']['email'] ?></span><?php } ?></label>
                    <input type="email" class="form-control" id="email" name="email">

                </div>
                <div class="form-group">

                    <label for="name">Name <?php if (isset($_SESSION['validationErrors']['name'])) { ?>
                            <span class="alert alert-danger text-center  p-1"><?= $_SESSION['validationErrors']['name'] ?></span><?php } ?></label>
                    <input type="text" class="form-control" id="name" name='name'>
                </div>
                <div class="form-group">

                    <label for="password">Password <?php if (isset($_SESSION['validationErrors']['password'])) { ?>
                            <span class="alert alert-danger text-center  p-1"><?= $_SESSION['validationErrors']['password'] ?></span><?php } ?></label>
                    <input type="password" class="form-control" id="password" name='password'>
                </div>
                <div class="form-group">

                    <label for="repeatPassword">Repeat password <?php if (isset($_SESSION['validationErrors']['password2'])) { ?>
                            <span class="alert alert-danger text-center  p-1"><?= $_SESSION['validationErrors']['password2'] ?></span><?php } ?></label>
                    <input type="password" class="form-control" id="repeatPassword" name='repeat_password'>
                </div>
                <div class="form-group">

                    <label for="type_id">User Type <?php if (isset($_SESSION['validationErrors']['type_id'])) { ?>
                            <span class="alert alert-danger text-center  p-0"><?= $_SESSION['validationErrors']['type_id'] ?></span><?php } ?></label>
                    <select class="form-control" id="type_id" name="type_id">
                        <option value='0' hidden>Choose User Type</option>

                        <?php
                        if(isset($_SESSION['types'])){
                        foreach($_SESSION['types'] as $type){?>
                            <option value="<?=$type->id?>"><?= $type->name?></option>

                        <?php }  }?>
                    </select>
                </div>
                <div class='d-flex justify-content-between'>
                    <a href="/home" class='btn btn-info '>Go Back</a>
                    <button type="submit" class="btn btn-success">Register</button>

                </div>

            </form>
        </div>
    </div>
</div>
<?php 

unset($_SESSION['validationErrors']);
unset($_SESSION['types']); ?>
<?php require_once __DIR__ . "/layouts/footer.php"; ?>