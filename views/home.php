<?php require_once __DIR__ . "/layouts/head.php";error_reporting(0); ?>

<div class="container my-5">
    <div class="row">
        <div class="col-12 my-5">
            <div class="text-center ">
                <a href="<?=isset($_SESSION['user']) ? '/logout':'/login';?>" class="btn btn-primary btn-lg mx-4"><?=isset($_SESSION['user']) ? 'Logout':'Login';?></a>
                <a href="/register" class="btn btn-primary btn-lg mx-4">Register</a>
            </div>
        </div>
        <div class="col-12 offset-0 col-md-8 offset-md-2">
            <!-- search form -->
            <form action='/results' method='GET'>
                <div class="form-row align-items-center">
                    <div class="form-group col-md-5">
                        <input type="text" class="form-control" id="searchBar" name="search" placeholder="Search">
                    </div>
                    <div class="form-group col-md-5">
                        <select class="form-control" name='type_id' id="type_id">
                            <option value='0' hidden>Chose user Type</option>
                            <?php foreach ($_SESSION['mainTypes'] as $type) { ?>
                                <option value='<?= $type->id ?>'><?= $type->name ?></option>
                            <?php } ?>
                        </select>
                    </div>
                    <div class="form-group col-md-2">
                        <button type="submit" class="btn btn-outline-success ">Search</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>




<?php require_once __DIR__ . "/layouts/footer.php"; ?>