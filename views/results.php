<?php require_once __DIR__ . "/layouts/head.php";error_reporting(0);?>

<a href="<?=isset($_SESSION['user']) ? '/logout':'/login';?>" class="btn btn-primary btn-lg mx-4"><?=isset($_SESSION['user']) ? 'Logout':'Login';?></a>
<div class="container my-5 ">
    <div class="row mb-3">
        <div class="col-12 text-center">
            <h3>Results</h3>
        </div>
    </div>
    <div class="row border-top p-3">
        <div class="col-12 offset-0 col-md-5 offset-md-1  ">
            <h4 class='text-center'>All User Types</h4>

            <ul id="myUL">
                <?php
                if(isset($_SESSION['results']['allResults'])){
                    $data=$_SESSION['results']['allResults'];
                }
                foreach ($data['main'] as $mainType) {
                    echo "<li><span class='caret $mainType->name'>$mainType->name($mainType->numOfUsers)</span><ul class='nested'>";
                    foreach($data['mid'] as $midType){
                        if ($midType->mainType == $mainType->id) {
                            echo "<li><span class='caret $midType->name'>$midType->name($midType->numOfUsers)</span><ul class='nested'>";
                            foreach ($data['sub'] as $subType) {
                                if ($subType->belongesToType == $midType->id) {
                                    echo "<li><span class='caret $subType->name'>$subType->name($subType->numOfUsers)<span><ul class='nested'>";
                                    foreach ($data['min'] as $minType) {
                                        if ($minType->belongesToSubType == $subType->id) {
                                            echo "<li><span class='$minType->name'>$minType->name($minType->numOfUsers)</span></li>";
                                        }
                                    }
                                    echo "</ul></li>";
                                }
                            }
                            echo "</ul></li>";
                    }
                }
            
                echo "</ul> </li>";
            }
                ?>
            </ul>

        </div>
        <?php      if(isset($_SESSION['results']['searchResults'])){
                    $searchResults=$_SESSION['results']['searchResults'];
               ?>
        <div class="col-12 offset-0 col-md-5 bored ">
            <h4 class='text-center'>Search Results</h4>
                <table class="table">
                <thead>
                    <th>Id</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Type</th>
                </thead>
                <tbody>
                <?php foreach($searchResults as $key => $result){?>
                    <tr>
                    <td><?=$key+1?></td>
                    <td><?=$result->name?></td>
                    <td><?=$result->email?></td>
                    <td><?=$result->type?></td>
                    </tr>
                <?php } ?>
                
                </tbody>
                
                </table>
        </div>
        <?php } ?>
    </div>
</div>
<?php 

if(isset($_SESSION['results'])){
    unset($_SESSION['results']);
    unset($_SESSION['mainTypes']);
 
}
?>
<script>
    let toggler = document.getElementsByClassName("caret");
    let i;

    for (i = 0; i < toggler.length; i++) {
        toggler[i].addEventListener("click", function() {
            this.parentElement.querySelector(".nested").classList.toggle("active");
            this.classList.toggle("caret-down");
        });
    }
</script>

<?php require_once __DIR__ . "/layouts/footer.php"; ?>