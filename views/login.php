<?php require_once __DIR__ . "/layouts/head.php";error_reporting(0); ?>

<?php
if (isset($_SESSION['successMsg'])) {
  
    $successMsg = $_SESSION['successMsg'];
}
?>
<div class="container my-5">
    <div class="row">
        <div class="col-12 offset-0 col-md-6 offset-md-3  justify-content-center  ">
            <?php

            if (isset($_SESSION['error'])) {
                $errorMsg = $_SESSION['error'];
                echo " <div class='alert alert-danger'>$errorMsg</div>";
            }
            if (isset($_SESSION['successMsg'])) {
                $successMsg = $_SESSION['successMsg'];
                echo "<div class='alert alert-success'>$successMsg</div>";
            }

            ?>
        </div>

        <div class="col-12 offset-0 col-md-6 offset-md-3 border px-5 pb-5 shadow">
            <h3 class='text-center my-3'>Login</h3>
            <!--  Login form -->
            <form action="/login/user" method="POST">
                <div class="form-group">
                    <label for="email">Email address</label>
                    <input type="email" class="form-control" id="email" name='email'>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" id="password" name='password'>
                </div>
                <div class='d-flex justify-content-between'>

                    <a href="/home" class='btn btn-info '>Go Back</a>
                    <button type="submit" class="btn btn-primary">Login</button>
                </div>

            </form>
        </div>
    </div>
</div>

<?php

if (isset($_SESSION['error'])) {
    unset($_SESSION["error"]);
}
if (isset($_SESSION['successMsg'])) {
    unset($_SESSION["successMsg"]);
}
?>
<?php require_once __DIR__ . "/layouts/footer.php"; ?>