<?php require_once __DIR__ . "/layouts/head.php"; ?>
<?php
session_start();
if (isset($_SESSION['user'])) {
    $user = $_SESSION['user'];
}
?>
 <a href="<?=isset($_SESSION['user']) ? '/logout':'/login';?>" class="btn btn-primary btn-lg mx-4"><?=isset($_SESSION['user']) ? 'Logout':'Login';?></a>
<div class="container mt-5">
    <div class="row">
        <div class="col">
            <a href="/home" class='btn btn-primary'>Home</a>
            <a href="/results" class='btn btn-primary'>Results</a>
        </div>
    </div>
    <div class="row">
        <div class="col-12 offset-0 col-md-6 offset-3">
            <div class="border shadow p-5 text-center">
                <h3>Welcome, <?= $user->name ?>!</h3>
            </div>
        </div>
    </div>

</div>

<?php require_once __DIR__ . "/layouts/footer.php"; ?>